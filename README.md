# EmotionDetection

Detect facial expressions through a convolutional network.

# Data Used
The 'The Japanese Female Facial Expression (JAFFE) Dataset' is a collection of black and white pictures (256x256px) of female japanese students.
Every picture has 6 attirbutes on a scale von 1 to 5:
- Happiness
- Sadness
- Surprise
- Anger
- Disgust
- Fear

The Model performs well evaluating the Dataset, but struggles to evaluate 'faces in thew wild' (for example web cam footage).

# Model Used

- Layer 1:
    - Convolutional Layer
    - (Max) Pooling Layer
- Layer 2:
    - Convolutional Layer
    - (Max) Pooling Layer
- Flatten/Dropout Layer:
    - Flatten
    - Dropout
- DenseLayer:
    - 512 Dense Layer
    - 256 Dense Layer
- OutputLayer:
    - logits

# Using Webcam to Evaluate

Just run `python cam.py` to extract face from your webcam and evaluate it.

