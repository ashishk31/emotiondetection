import tensorflow as tf
import numpy as np

#
# Neural Network Construction
#

n_attributes = 6

# Variablen
X = tf.placeholder(tf.float32, shape=(None, 256, 256, 1), name='X')
y = tf.placeholder(tf.float32, shape=(None, 6), name='y')

# DNN Layers
with tf.name_scope('Layer1'):

    conv1 = tf.layers.conv2d(
        X,
        filters=16,
        kernel_size=6,
        strides=[2, 2],
        padding='SAME',
        activation=tf.nn.relu
    )

    pool1 = tf.layers.max_pooling2d(
        conv1,
        2,
        2,
        padding='VALID'
    )

with tf.name_scope("Layer2"):
    conv2 = tf.layers.conv2d(
        pool1,
        filters=32,
        kernel_size=3,
        strides=[2, 2],
        padding='SAME',
        activation=tf.nn.relu
    )

    pool2 = tf.layers.max_pooling2d(
        conv2,
        2,
        2,
        padding='VALID'
    )

with tf.name_scope("FlattenDropoutLayer"):
    flatten = tf.layers.flatten(pool2)
    dropout = tf.layers.dropout(flatten, 0.6)

with tf.name_scope("DenseLayer"):
    dense1 = tf.layers.dense(dropout, 512, activation=tf.nn.relu)
    dense2 = tf.layers.dense(dense1, 256, activation=tf.nn.relu)

with tf.name_scope("OutputLayer"):
    logits = tf.layers.dense(dense2, n_attributes,
                             name='logits', reuse=tf.AUTO_REUSE)

# Loss and Train

with tf.name_scope('loss'):
    loss = tf.losses.mean_squared_error(y, logits)

with tf.name_scope('train'):
    optimizer = tf.train.AdamOptimizer(0.001)
    training_op = optimizer.minimize(loss)

# Session

init = tf.global_variables_initializer()
saver = tf.train.Saver()


def eval_face(image):
    formatted = np.array(image).reshape((-1, 256, 256, 1)).astype(np.float32)

    with tf.Session() as sess:
        saver.restore(sess, './model/v1-prototype.ckpt')

        result = logits.eval(feed_dict={X: formatted})
        # print(result.shape)
        return result[0]
